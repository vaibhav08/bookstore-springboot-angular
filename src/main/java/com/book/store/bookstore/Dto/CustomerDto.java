package com.book.store.bookstore.Dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CustomerDto implements Serializable {
    private int id;
    private String name;
    private String email;
    private String saltedPassword;
    private String address;
}
