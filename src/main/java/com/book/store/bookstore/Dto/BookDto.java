package com.book.store.bookstore.Dto;

import lombok.Data;

@Data
public class BookDto {
    private int id;
    private String name;
    private String author;
    private String publication;
    private int availableQuantity;
    private long price;
}
