package com.book.store.bookstore.Controller;
import com.book.store.bookstore.Dto.CustomerDto;
import com.book.store.bookstore.Dto.LoginDto;
import com.book.store.bookstore.Entity.Customer;
import com.book.store.bookstore.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @RequestMapping(method = RequestMethod.POST, value = "/customer")
    public String addCustomer(@RequestBody CustomerDto customerDto){
        Customer customer = new Customer(customerDto.getName(),customerDto.getEmail(),customerDto.getSaltedPassword(),customerDto.getAddress());
        return customerService.addCustomer(customer);
    }
    @RequestMapping(method = RequestMethod.POST, value="/login")
    public Optional<Customer> getCustomerFromEmailAndPassword(@RequestBody LoginDto loginDto){
        return customerService.getCustomerFromEmailAndPassword(loginDto.getEmail(),loginDto.getPassword());
    }
    @RequestMapping(method= RequestMethod.GET , value = "/customer")
    public List<Customer> getAllCustomer(){
        return customerService.getAllCustomer();
    }
    @RequestMapping(method = RequestMethod.GET , value="/customer/{id}")
    public Optional<Customer> getCustomerById(@PathVariable("id")int id){
        return customerService.getCustomerById(id);
    }
    @RequestMapping(method =RequestMethod.PUT, value = "/customer")
    public String updateCustomer(@RequestBody CustomerDto customerDto){
        Customer customer = new Customer(customerDto.getId(),customerDto.getName(),customerDto.getEmail(),customerDto.getSaltedPassword(),customerDto.getAddress());
        return customerService.updateCustomer(customer);
    }
    @RequestMapping(method = RequestMethod.DELETE , value="/customer/{id}")
    public String deleteCustomer(@PathVariable("id")int id){
        return customerService.deleteCustomer(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/customer/{customerId}/book/{bookId}")
    public Map<String,String> addBookToCart(@PathVariable("customerId")int customerId, @PathVariable("bookId")int bookId){
        Map<String,String> map = new HashMap<>();
        map.put("msg",customerService.addBookToCart(customerId,bookId));
        return map;
    }
    @RequestMapping(method = RequestMethod.DELETE , value="/customer/{customerId}/book/{bookId}")
    public Map<String, String> deleteBookFromCart(@PathVariable("customerId")int customerId, @PathVariable("bookId")int bookId){
        Map<String,String> map = new HashMap<>();
        map.put("msg", customerService.deleteBookFromCart(customerId,bookId));
        return map;
    }
    @RequestMapping(method = RequestMethod.DELETE, value="/customer/{customerId}/book/reduce/{bookId}")
    public Map<String, String> reduceBookQuantityFormCart(@PathVariable("customerId")int customerId, @PathVariable("bookId")int bookId){
        Map<String,String> map = new HashMap<>();
        map.put("msg",customerService.reduceBookQuantityFromCart(customerId,bookId));
        return map;
    }
    @RequestMapping(method = RequestMethod.PUT, value="/customer/{customerId}/order")
    public Map<String,String> makeOrder(@PathVariable("customerId")int customerId){
        Map<String,String> map = new HashMap<>();
        map.put("msg",customerService.makeOrder(customerId));
        return map;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/customer/{customerId}/order")
    public List getCustomerOrders(@PathVariable("customerId")int customerId){
        return customerService.getCustomerOrders(customerId);
    }
    @RequestMapping(method = RequestMethod.GET, value="/customer/{customerId}/cart")
    public Map getCustomerCartItems(@PathVariable("customerId")int customerId){
        return customerService.getCustomerCartItems(customerId);
    }
    @RequestMapping(method = RequestMethod.DELETE, value="/customer/{customerId}/order")
    public String deleteCustomerOrder(@PathVariable("customerId")int customerId){
        return customerService.deleteCustomerOrder(customerId);
    }
    @RequestMapping(method = RequestMethod.DELETE , value="/customer/{customerId}/order/{orderId}")
    public String deleteCustomerOrderFromOrderId(@PathVariable("customerId")int customerId,@PathVariable("orderId")int orderId){
        return customerService.deleteCustomerOrderFromOrderId(customerId,orderId);
    }
}
