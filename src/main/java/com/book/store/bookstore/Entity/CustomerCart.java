package com.book.store.bookstore.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Data
public class CustomerCart {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private int id;
    @JsonIgnore
    @OneToOne(mappedBy = "customerCart")
    private Customer customer;
    @ElementCollection
    @Column(name="value")
    @CollectionTable(name="cartbookvaluetable", joinColumns=@JoinColumn(name="cart_id"))
    private Map<Integer,Integer> bookIntegerMap=new HashMap<>();
    public CustomerCart(){}

}
